
mapper = map [1..10]

data Sum a b =
  First a
  | Second b
  deriving (Eq, Show)
