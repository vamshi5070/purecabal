import Control.Monad

data Sum a b =
  First a
  | Second b
  deriving (Eq, Show)

instance Functor (Sum a) where
  fmap f (First a) = First a
  fmap f (Second b) = Second $ f b

instance Applicative (Sum a) where
  pure  = Second
  (<*>) (First f) _ = First f
  (<*>) _ (First b) = First b
  (<*>) (Second f) (Second a) = Second $ f a

instance Monad (Sum a) where
  return = Second
  (>>=) (First a) _ = First a
  (>>=) (Second a) f = f a

mass = do
  print $ 3
