multiplesOf n limit = takeWhile (<limit) $ multiplesInf n

multiplesInf n = n:map (+n) (multiplesInf n)

sumOfbelowXOry x y limit = sum (multiplesOf x limit) + sum (multiplesOf y limit) - sum (multiplesOf (x*y) limit)

fibs = 1 : 2 : zipWith (+) fibs (tail fibs)

summer  = sum [x | x <- [1..1000] , x `mod` 3 == 0 || x `mod` 5 == 0 ]
