import Test.Hspec
import Test.QuickCheck

main :: IO ()
main = hspec $ do
  describe "Addition" $ do
    it "1 + 1 is greater than 1" $ do
      (1 + 1) > 1 `shouldBe` True

mainWithProp :: IO ()
mainWithProp = hspec $ do
  describe "Add" $ do
    it "x + 1 is always\
       \ greater than x" $ do
      property $ \x -> x + 1 > (x :: Int)

recSum :: (Ord a, Num a) => a -> a -> a
recSum x y
  | y <= 0 = 0
  | y == 1 = x
  | otherwise = x + recSum x (y-1)
-- recSum _ 0 = 0
-- recSum x 1 = x
-- recSum x y = x + recSum x (y-1)

tester :: IO ()
tester  = hspec $ do
  describe "Multiplication recSum" $ do
    it "4*7 = 28" $ do
      recSum 4 7 `shouldBe` 28
    it "4*17 = 68" $ do
      recSum 4 17 `shouldBe` 68

prop_recSum :: Integer -> Integer -> Bool
prop_recSum x y = recSum (abs x) (abs y) == abs x*y
