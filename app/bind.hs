import Control.Monad

-- bind :: Monad m => m a -> (a -> m b) -> m b
bind :: Monad m => (a -> m b) -> m a -> m b
bind f  = join . fmap f
