import Control.Monad.Reader
import Control.Monad.Writer

-- reader creates a immutable read only state which can be treated as a global variable in haskell

data Environment = Environment
  { param1 :: String
  , param2 :: String
  , param3 :: String
  }

loadEnv :: IO Environment
loadEnv = do
  return $ Environment {param1 = "vamshi",param2 = "emacs",param3 = "skip"}

main :: IO ()
main = do
  env <- getLine
  let ret = runReader computation env 
  putStrLn ret
  lenv <- getLine
  let (rett,accc) = runWriter  writeCompute 
  putStrLn $ "Accc is " <> accc
  putStrLn $ "Ret is " <> rett

computation :: Reader String String 
computation = do
  env <- ask
  intVal <- intCompute 
  return $ "Int value: " <> show intVal

intCompute :: Reader String Int
intCompute = do
  env <- ask
  return $ length env

writeCompute :: Writer String String
writeCompute = do
    tell "start acc"
    return "Writer"

yawriteComp :: Writer String Int
yawriteComp = do
    str<- writeCompute
    tell "also acccc"
    return 4
