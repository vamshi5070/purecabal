import Control.Monad.State
import System.Random

test :: State Int Int
test = do
  put 3
  modify (+1)
  get

main :: IO ()
main = print $ runState test 0

rodMain :: IO()
rodMain = do
  x <- getLine
  putStrLn x

desugared = getLine >>= putStrLn

postincrement ::  State Int Int
postincrement = do { x <- get; put (x+1); return x }

postincrement' :: State Int Int
postincrement' = get  >>=  \x -> put (x+1) >>= \_ -> return x

predecrement ::  State Int Int
predecrement = do { x <- get; put (x-1); get }

predecrement' ::  State Int Int
predecrement' =   get >>= \x -> put (x-1) >>= \_ ->  get


-- myMap :: [a] -> [(a,Int)]
-- myMap = map (\(x,_) -> x+2)
