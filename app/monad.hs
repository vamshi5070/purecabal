data Nope a = NopeDotJpg

instance Functor Nope where
  fmap f NopeDotJpg = NopeDotJpg

instance Applicative Nope where
  pure _  = NopeDotJpg
  -- (<*>) _ NopeDotJpg = NopeDotJpg
  (<*>) NopeDotJpg _ = NopeDotJpg

instance Monad Nope where
  return  = pure
  (>>=) NopeDotJpg _ = NopeDotJpg

newtype Identity a = Identity a
  deriving (Eq, Ord, Show)

instance Functor Identity where
  fmap f (Identity a) = Identity $ f a

instance Applicative Identity where
  pure = Identity
  (<*>) (Identity f) (Identity a) = Identity $ f a 

instance Monad Identity where
  return = pure
  (>>=) (Identity a) f = f a 

data List a =
  Nil
  | Cons a (List a)
  deriving (Eq,Show)

instance Functor List where
  fmap _ Nil = Nil
  fmap f (Cons a xs) =  Cons (f a) (fmap f xs)

instance Applicative List where
  pure a = Cons a Nil
  (<*>) _ Nil = Nil
  -- (<*>) (Cons )
