data TurnstileState = Locked | Unlocked
                    deriving (Eq, Show)

data TurnstileOutput = Thank | Open | Tut
                    deriving (Eq, Show)

push :: TurnstileState -> (TurnstileOutput,TurnstileState)
push Unlocked = (Open,Locked)
push Locked = (Tut,Locked)

coin :: TurnstileState -> (TurnstileOutput,TurnstileState)
coin _ = (Thank,Unlocked)

monday :: TurnstileState -> ([TurnstileOutput], TurnstileState)
monday s0 =
  let (a1,s1) = push s0
      (a2,s2) = coin s1
      (a3,s3) = push s2
      (a4,s4) = push s3
      (a5,s5) = coin s4
      (a6,s6) = coin s5
  in ([a1,a2,a3,a4,a5,a6],s6)

regularPerson, distractedPerson --, hastyPerson
  :: TurnstileState -> ([TurnstileOutput], TurnstileState)
regularPerson s0 =
  let (a1,s1) = coin s0
      (a2,s2) = push s1
  in ([a1,a2],s1)

distractedPerson s0 =
  let (a1,s1) = coin s0
  in ([a1],s1)
-- hastyPerson s0
  -- | a1 == Open = 
    -- where (a1,modifiedState) = push s0
